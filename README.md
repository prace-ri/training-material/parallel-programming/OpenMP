# Parallel programming with OpenMP

This repository contains various exercises and examples on parallel programming with OpenMP.

A C or Fortran compiler supporting OpenMP is needed for building the code. Simple cases can
be built and run as:

 - gcc -o exe -fopenmp exercise.c 
 - gfortran -o exe -fopenmp exercise.c
 
where gcc/gfortran and -fopenmp should be replaced by proper compiler commands 
and options if you are not using the GNU compilers. 
For more complex cases a Makefile is provided.

## Exercises

 - [Hello world](hello-world) Simplest possible OpenMP program 
   (C and Fortran versions). Level: **basic**
 - [Parallel region and data sharing](data-sharing) The basic data sharing 
   primitives (C and Fortran versions). Level: **basic**
 - [Work sharing for a simple loop](work-sharing) Simple parallelization of
   for/do loop (C and Fortran versions). Level: **basic**
 - [Vector sum and race condition](race-condition) A basic example of race
   condition and various ways for resolving it (C and Fortran versions). 
   Level: **intemediate**
 - [Using OpenMP tasks for dynamic parallelization](tasks) Utilising OpenMP
   task construct for more dynamic parallelization (C and Fortran versions).
   Level: **intemediate**



## Examples
 - [Heat equation](heat-equation) A two dimensional heat equation solver which
   is parallelized with OpenMP. (C and Fortran versions). 
   Level: **intermediate** 

## How to contribute

Any contributions (new exercises and examples, bug fixes, improvements etc.) are
warmly welcome. In order to contribute, please follow the standard
Gitlab workflow:

1. Fork the project into your personal space
2. Create a feature branch
3. Work on your contributions
4. Push the commit(s) to your fork
5. Submit a merge request to the master branch

As a quality assurance, the merge request is reviewed by PRACE staff before it is accepted into main branch.

